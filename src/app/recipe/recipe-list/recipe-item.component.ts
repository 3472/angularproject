import { Component, OnInit, Input } from '@angular/core';
import { RecipeService } from "../recipe.service";

import { Recipe } from "../recipe.model";
@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styles: []
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe: Recipe;
  @Input() recipeId: number;

  constructor( private recipeService: RecipeService ) { }

  ngOnInit() {
  }

}
